
namespace IdentityProvider.Models.ViewModels
{
    public class RedirectViewModel
    {
        public string RedirectUrl { get; set; }
    }
}