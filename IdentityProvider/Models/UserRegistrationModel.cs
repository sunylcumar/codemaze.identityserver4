﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace IdentityProvider.Models
{
    public class UserRegistrationModel
    {
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Address1 is required")]
        [DisplayName("Address 1")]
        public string Address1 { get; set; }

        [DisplayName("Address 2")]
        public string Address2 { get; set; }

        [Required(ErrorMessage = "City is required")]
        [DisplayName("City")]
        public string City { get; set; }

        [Required(ErrorMessage = "State is required")]
        [DisplayName("State")]
        public string State { get; set; }

        [Required(ErrorMessage = "Zip is required")]
        [DisplayName("Postal Code")]
        public string Zip { get; set; }

        [Required(ErrorMessage = "Country is required")]
        [DisplayName("Country")]
        public string Country { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress]
        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }
    }
}
