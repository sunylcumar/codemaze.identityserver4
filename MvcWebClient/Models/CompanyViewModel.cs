﻿namespace MvcWebClient.Models
{
    public class CompanyViewModel
    {
        public string Name { get; set; }
        public string FullAddress { get; set; }
    }
}
