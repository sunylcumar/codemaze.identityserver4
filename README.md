# README #

Implementing SSO using IdentityServer4



### The solution contains the following ###

* IdentityServer4 is the IDP
* MvcWebClient is the ASP.Net Core MVC application which uses the IDP
* The projects under the folder Api are from the book example but slightly modified

### Reference used ###

* Mastering ASP.NET Core Security from CodeMaze

### Current Progress ###

* 11 User Registration with ASP.NET Core Identity

### Next topic to start ###

* 12 Reset Password with ASP.NET Core Identity
